package com.nCinga.interfaces;

interface m1 {
    default void eat(){
        System.out.println("Eating in m1");
    }
}

interface m2 {
    default void eat(){
        System.out.println("Eating in m2");
    }
}

class Test implements m1, m2 {

    public void eat() {
        m1.super.eat();
        m2.super.eat();
    }
}

public class Main {

    public static void main(String[] args) {

        Test test = new Test();
        test.eat();

    }
}
