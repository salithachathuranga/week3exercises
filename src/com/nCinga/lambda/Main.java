package com.nCinga.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

interface OddEvenChecker{
    boolean checkNumber(int number);
}

public class Main {

    public static void main(String[] args) {

        List<Integer> list = Arrays.asList(1,2,3,4,5,6,7,8);
        List<Integer> evenList = new ArrayList<>();

        OddEvenChecker checker = x -> x % 2 == 0;

        for (Integer i:list) {
            if(checker.checkNumber(i)){
                evenList.add(i);
            }
        }

        System.out.println(evenList);
    }
}
