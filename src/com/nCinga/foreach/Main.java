package com.nCinga.foreach;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class Main {

    public static void concepts(String[] args) {
        List<String> names = Arrays.asList("Sam", "Bob", "Sim");
        names.forEach(a -> System.out.println(a));

        Consumer<String> changeCase = a -> System.out.println(a.toUpperCase());
        names.forEach(changeCase);

        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(1, "a");
        hm.put(2, "b");
        hm.put(3, "c");
        hm.put(4, "d");

        hm.forEach((k, v) -> System.out.println("Key = "+ k + " Value = "+ v));
        BiConsumer<Integer, String> biConsumer = (k, v) -> {
            System.out.println("Within lambda block");
            System.out.println(" Modified key = "+ (k+10) + "Value = "+ v.toUpperCase());
        };
        hm.forEach(biConsumer);

        System.out.println("Entry set");
        Consumer<Map.Entry<Integer, String>> mapConsumer = System.out:: println;
        Consumer<Integer> keyConsumer = System.out:: println;
        hm.entrySet().forEach(mapConsumer);
        hm.keySet().forEach(keyConsumer);

    }

    static void checkPalindromeString(List<String> words){
        Consumer<String> consumer = s -> {
            StringBuilder reversedStr = new StringBuilder(s).reverse();
            if (reversedStr.toString().equals(s)){
                System.out.println("This is a palindrome");
            }
            else {
                System.out.println("This is not a palindrome");
            }
        };
        words.forEach(consumer);
    }

    static void checkPalindromeStringWithLoop(List<String> words){
        Consumer<String> consumer = s -> {
            StringBuilder checkStr = new StringBuilder();
            for(int i = s.length()-1; i >= 0 ; i--) {
                checkStr.append(s.charAt(i));
            }
            if (checkStr.toString().equals(s)){
                System.out.println("This is a palindrome");
            }
            else {
                System.out.println("This is not a palindrome");
            }
        };
        words.forEach(consumer);
    }

    public static void main(String[] args) {
        List<String> words = Arrays.asList("abc","aba", "aaa", "abcba", "bcbd");
        checkPalindromeStringWithLoop(words);
    }
}
