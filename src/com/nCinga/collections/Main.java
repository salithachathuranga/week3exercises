package com.nCinga.collections;

import java.util.*;

public class Main {

    static void removeDuplicates(List<Integer> list){
        List<Integer> newList = new ArrayList<>();
        for (Integer i: list) {
            if(!newList.contains(i)){
                newList.add(i);
            }
        }
        System.out.println(newList);
    }

    static void mergeTwoLinkedLists(LinkedList<Integer> list1, LinkedList<Integer> list2){
        list1.addAll(list2);
        Collections.sort(list1);
        System.out.println(list1);
    }

    static void sortByFrequency(ArrayList<Integer> list){
        LinkedHashMap<Integer,Integer> map = new LinkedHashMap<>();
        for(Integer i: list){
            if(map.get(i) == null){
                map.put(i, 1);
            }
            else {
                int updatedCount = map.get(i)+1;
                map.replace(i,updatedCount);
            }
        }
        LinkedList<Integer> orderedList = new LinkedList<>();
        for(Map.Entry<Integer,Integer> entry: map.entrySet()){
            if(entry.getValue() == 1){
                orderedList.add(entry.getKey());
            }
            else {
                for (int i = 0; i < entry.getValue(); i++) {
                    orderedList.addFirst(entry.getKey());
                }
            }
        }
        System.out.println(orderedList);

    }

    static String checkParenthesis(String expression){
        Stack<Character> stack = new Stack<>();
        if (expression.isEmpty()){
            return "Balanced";
        }
        for (int i = 0; i < expression.length(); i++) {
            char currentChar = expression.charAt(i);
            if (currentChar == '{' || currentChar == '(' || currentChar == '['){
                stack.push(currentChar);
            }
            else if (currentChar == '}' || currentChar == ')' || currentChar == ']'){
                char lastChar = stack.peek();
                if (
                    (currentChar == '}' && lastChar == '{') ||
                    (currentChar == ')' && lastChar == '(') ||
                    (currentChar == ']' && lastChar == '[')
                ){
                    stack.pop();
                    System.out.println(stack);
                }
                else{
                    return "Not Balanced";
                }
            }
        }
        return stack.isEmpty() ? "Balanced" : "Not Balanced";

    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(2);
//        removeDuplicates(list);

        LinkedList<Integer> list1 = new LinkedList<>();
        list1.add(1);
        list1.add(3);
        list1.add(5);
        list1.add(7);

        LinkedList<Integer> list2 = new LinkedList<>();
        list2.add(2);
        list2.add(4);
        list2.add(6);
        list2.add(8);

//        mergeTwoLinkedLists(list1,list2);

        ArrayList<Integer> list3 = new ArrayList<>();
        list3.add(1);
        list3.add(5);
        list3.add(3);
        list3.add(6);
        list3.add(5);
        list3.add(2);
        list3.add(3);
        list3.add(7);
        list3.add(3);
        list3.add(8);
        list3.add(3);
//        System.out.println(list3);

        //[1, 5, 3, 6, 5, 2, 3, 7, 3, 8, 3]
        //[3, 3, 3, 3, 5, 5, 1, 6, 2, 7, 8]
//        sortByFrequency(list3);

        // LIFO
        Stack<Integer> stack = new Stack<>();
        stack.push(5);
        stack.push(3);
        stack.push(1);
        stack.push(4);
        stack.push(2);
//        System.out.println(stack);
//        System.out.println(stack.pop());
//        System.out.println(stack);
//        System.out.println(stack.peek());
//        System.out.println(stack);

        Queue<Integer> queue = new LinkedList<>();
        queue.add(1);
        queue.add(3);
        queue.add(5);
        queue.add(2);
        queue.add(4);
//        System.out.println(queue.peek());
//        System.out.println(queue.poll());
//        System.out.println(queue.poll());

        System.out.println(checkParenthesis("[()]{}{[()()]()}"));


        Movie movie1 = new Movie("Angry Birds", 7.5, 2010);
        Movie movie2 = new Movie("Transformers", 5.0, 2011);
        Movie movie3 = new Movie("How To Train Your Dragon", 9.1, 2014);
        Movie movie4 = new Movie("Cats and Dogs", 8.2, 2008);
        List<Movie> movies = new ArrayList<>();
        movies.add(movie1);
        movies.add(movie2);
        movies.add(movie3);
        movies.add(movie4);
//        Collections.sort(movies);
//        System.out.println(movies);
//        Collections.sort(movies, new ReleaseYearComparator());
//        System.out.println(movies);
//        Collections.sort(movies, new RatingComparator());
//        System.out.println(movies);

        Queue<Movie> movieQueue = new PriorityQueue<>(new RatingComparator());
        movieQueue.add(movie1);
        movieQueue.add(movie2);
        movieQueue.add(movie3);
        movieQueue.add(movie4);
        System.out.println(movieQueue);
        System.out.println(movieQueue.peek());
        System.out.println(movieQueue.poll());
        System.out.println(movieQueue);

//        Map<String,Integer> map = new HashMap<>();
//        map.put("abc", 1);
//        map.put("def", 2);
//        map.put("asd", 3);
//        System.out.println(map);

    }
}
