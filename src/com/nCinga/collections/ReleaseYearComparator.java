package com.nCinga.collections;

import java.util.Comparator;

public class ReleaseYearComparator implements Comparator<Movie> {
    @Override
    public int compare(Movie movie1, Movie movie2) {
        return movie1.getReleaseYear() - movie2.getReleaseYear();
    }
}
