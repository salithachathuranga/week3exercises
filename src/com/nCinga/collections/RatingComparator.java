package com.nCinga.collections;

import java.util.Comparator;

public class RatingComparator implements Comparator<Movie> {
    @Override
    public int compare(Movie movie1, Movie movie2) {
        if (movie1.getRating() - movie2.getRating() == 0){
            return 0;
        }
        else if (movie1.getRating() - movie2.getRating() > 0){
            return 1;
        }
        else {
            return -1;
        }
    }
}
