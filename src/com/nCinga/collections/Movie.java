package com.nCinga.collections;

import java.util.Comparator;
import java.util.Objects;

public class Movie implements Comparable<Movie> {

    private String name;
    private Double rating;
    private Integer releaseYear;

    public Movie(String name, Double rating, Integer releaseYear) {
        this.name = name;
        this.rating = rating;
        this.releaseYear = releaseYear;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return name.equals(movie.name) &&
                rating.equals(movie.rating) &&
                releaseYear.equals(movie.releaseYear);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, rating, releaseYear);
    }

    @Override
    public int compareTo(Movie movie) {
        return this.name.compareTo(movie.name);
    }

    @Override
    public String toString() {
        return "Movie{name=" + name + ", rating=" + rating + ", releaseYear=" + releaseYear + "}";
    }
}
