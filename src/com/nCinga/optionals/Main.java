package com.nCinga.optionals;


import java.util.Optional;
import java.util.function.Predicate;

class Employee {

    String name;
    Double salary;

    public Employee(String name, Double salary) {
        this.name = name;
        this.salary = salary;
    }

    public String toString(){
        return "{ "+name+", "+salary+" }";
    }
}

public class Main {

    public static void main(String[] args) {

        Optional<Employee> empOpt1 = Optional.empty();
        Optional<Employee> empOpt2 = Optional.of(new Employee("john", 10000.00));
        Predicate<Employee> p = x ->  (x.name.startsWith("j"));
        System.out.println(empOpt1.filter(p));
        System.out.println(empOpt2.filter(p));
        System.out.println(empOpt1.orElse(new Employee("adam", 20000.00)));
        System.out.println(empOpt1.isPresent());
        System.out.println(empOpt2.isPresent());
        System.out.println(empOpt2.get());

   }
}
