package com.nCinga.streams;

import java.util.Collections;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    static void sumOfElementsInStream(Stream<Integer> stream){

        Integer sum = stream.reduce((a, b) -> a + b).orElse(0);
        System.out.println(sum);
//        System.out.println(stream.mapToInt(Integer::intValue).sum());
//        Integer sum = stream.collect(Collectors.summingInt(Integer::intValue));
//        System.out.println(sum);

    }

    static void reverseElementsInStream(Stream<String> stream){
        stream.sorted(Collections.reverseOrder()).forEach(System.out::println);
    }

    public static void main(String[] args) {

        Stream<Integer> s1 = Stream.of(1, 2, 3, 4, 5);
        Stream<String> s2 = Stream.of("Hello", "Hi", "Sam");

        sumOfElementsInStream(s1);
        reverseElementsInStream(s2);
    }
}
