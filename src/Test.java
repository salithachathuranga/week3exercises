import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

interface Square{
    int getArea(int side);
}

class Employee {
    private String name;
    private String designation;
    private int salary;

    public Employee(String name) {
        this.name = name;
    }

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public Employee(String name, String designation, int salary) {
        this.name = name;
        this.designation = designation;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", designation='" + designation + '\'' +
                ", salary=" + salary +
                '}';
    }
}

public class Test {

    public static void main(String[] args) {

        ////// LAMBDA EXPRESSIONS //////
        Square square = side -> (side*side);
        System.out.println(square.getArea(5));

        List<Employee> employees = Arrays.asList(
                new Employee("salitha", "SE", 45000),
                new Employee("lasitha", "BA", 5000),
                new Employee("roshan", "QA", 10000),
                new Employee("yasas", "SE", 25000),
                new Employee("logee", "SE", 35000)
        );

        ////// FOREACH //////
        Consumer<Employee> consumer = employee -> {
            if(employee.getDesignation().equals("SE")){
                System.out.println("selected");
            }
        };
        employees.forEach(consumer);
        employees.forEach(employee -> System.out.println(employee.getName()));

        Map<Integer, Employee> employeeMap = new HashMap<>();
        employeeMap.put(1, new Employee("salitha", "SE", 45000));
        employeeMap.put(2, new Employee("lasitha", "BA", 5000));
        employeeMap.put(3, new Employee("roshan", "QA", 10000));
        employeeMap.put(4, new Employee("yasas", "SE", 25000));
        employeeMap.put(5, new Employee("logee", "SE", 35000));

        employeeMap.forEach((k, v) -> System.out.println("Key = "+ k + " Value = "+ v));

        BiConsumer<Integer, Employee> biConsumer = (k, v) -> {
            System.out.println("Key = "+ k + " | " + "Value = "+ v.toString());
        };
        employeeMap.forEach(biConsumer);

        Consumer<Map.Entry<Integer, Employee>> mapConsumer = System.out::println;
        Consumer<Integer> keyConsumer = System.out::println;
        Consumer<Employee> valueConsumer = System.out::println;
        employeeMap.entrySet().forEach(mapConsumer);
        employeeMap.keySet().forEach(keyConsumer);
        employeeMap.values().forEach(valueConsumer);

        ////// STREAMS API //////
        List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7,8,9);
        List<String> names = Arrays.asList("James", "Sam", "Adam", "Jones", "Rob", "Bobby");

        // FILTER - PREDICATE
        numbers.stream().filter(i -> i > 4).forEach(System.out::println);
        // MAP
        names.stream().filter(name -> !name.startsWith("J"))
                .map(name -> new Employee(name,10000))
                .forEach(System.out::println);      // name -> new Employee(name)

        List<Employee> list1 = names.stream().filter(name -> !name.startsWith("J")).map(Employee::new).collect(Collectors.toList());
        System.out.println(list1);

        List<Employee> list2 = names.stream()
                .filter(name -> !name.startsWith("J")).map(name -> new Employee(name,10000))
                .collect(Collectors.toList());
        System.out.println(list2);
        // SUM
        System.out.println(list2.stream().mapToInt(Employee::getSalary).sum());                         // user -> user.getSalary()
        // MAX
        System.out.println(list2.stream().mapToInt(Employee::getSalary).max().getAsInt());
        // AVERAGE
        System.out.println(list2.stream().mapToInt(Employee::getSalary).average().getAsDouble());
        // COUNT
        System.out.println(list2.stream().mapToInt(Employee::getSalary).count());

        List<Employee> list3 = employees.stream()
                .filter(employee -> employee.getSalary() > 10000)
                .map(employee -> new Employee(employee.getName(), employee.getDesignation(), employee.getSalary()))
                .collect(Collectors.toList());

        // SORT
        /*list3.stream().sorted(new Comparator<Employee>() {
            @Override
            public int compare(Employee employee, Employee t1) {
                return employee.getSalary() - t1.getSalary();
            }
        }).forEach(System.out::println);*/

        list3.stream().sorted(Comparator.comparingInt(Employee::getSalary)).forEach(System.out::println);

        // DISTINCT
        List<Integer> numbers2 = Arrays.asList(1,2,5,7,3,4,5,1,1,4,6,7,8,9,3,3,6);
        System.out.println("========== remove duplicates ===========");
        numbers2.stream().distinct().forEach(System.out::print);

        // LIMIT
        System.out.println("\n========== limit records ===========");
        numbers2.stream().limit(3).forEach(System.out::print);
        System.out.println();

        // REDUCE
        Integer sum = numbers.stream().reduce(Integer::sum).orElse(0);       // (a, b) -> a + b
        System.out.println(sum);

    }
}
